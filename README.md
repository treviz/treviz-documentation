Treviz Documentation 
========================

![Treviz Logo](https://treviz.xyz/build/images/treviz-logo-circle.png)

## Description

Treviz is an open-source web platform that helps you set up and join open, collaborative organizations, in which you 
can work on the projects you want and get rewarded for your work.

This repository contains the documentation of the platform. It is maintained using [VuePress](https://vuepress.vuejs.org/)

Treviz is still in beta, so use it carefully.

## Contributing

Check out our [Contributing Guide](./CONTRIBUTING.md) if you are interested in joining the development team, if you want to submit a bug, etc.

## Public instances

The list of public Treviz instances is available on [our website](https://treviz.xyz/instances). If you want to
list your, simply add it to the form, we'll check it out and display it!

## Privacy

Privacy is one of our main concerns when it comes to developing Treviz. We do not sell any information about our users, nor
do we try to analyze their behaviour. If a security breach was to be found, we would hunt it down and fix it as soon as we could.

## Contact

You can learn more about Treviz on:
* our website: [treviz.xyz](https://treviz.xyz)
* our documentation: [doc.treviz.xyz](https://doc.treviz.xyz)
* our blog: [blog.treviz.xyz](https://blog.treviz.xyz)

Feel free to [contact us](https://treviz.xyz/contact) if you have any question.
