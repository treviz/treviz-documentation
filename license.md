# License

Copyright 2019 Bastien Huber

All the components of Treviz, including this documentation, are licensed under the Apache License, Version 2.0 \(the "License"\). You may not use this file except in complicance with the License. You may obtain a copy of the License at

`http://www.apache.org/licenses/LICENSE-2.0`

Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on a "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.



