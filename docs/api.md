# Treviz API

## Introduction

Treviz is built around an Angular5 application that consumes a REST API created with Symfony 3.3.

In this part, you will learn the specifications of the API, as well as how to consume it in your own application.Treviz is built around an Angular5 application that consumes a REST API created with Symfony 3.3. In this part, you will learn the specifications of the API, as well as how to consume it in your own application.

Each instance of Treviz exposes an endpoint that will allow use to browse and test its API. This documentation is available at: `/<api-version>/doc`

For instance, if a Treviz instance exposed its API at `api.treviz.org`, you could browse its documentation at `api.treviz.org/v1/doc`

More details coming soon.
