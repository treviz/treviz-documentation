# Contributing to Treviz

## Where to start

Thank you for your interest in Treviz! There are many ways you can help us making this platform better.

If you want to contribute to the project, we can only encourage you to join us by registering on the MatchMyProject website \(a public instance of Treviz\), and candidate to Treviz. This way, you will actually be rewarded for your work, and have access to all our Kanban boards, chatrooms, documents, etc. Just say you would love to contribute, specify your skills, preferences \(frontend, backend...\) and we will gladly welcome you !

You can also have a look at the To Do lists that are written in every readme of our repositories to find some general guidelines regarding the work ahead. We would also love to receive any other contribution, regarding:

* Translation of the various modules, documentation
* Improvements of our documentation
* Bug reports
* ...

## Bugs

If you have found a bug in Treviz, you can report it by opening an issue on our GitHub. Specify exactly how we can reproduce it, what is your setup \(operating system, browser, php version, etc\). The more information we have, the faster we can track and resolve it.

## License

Any contribution must be licensed under Apache License 2.0. Read our license, or ask us if you have any question.

## Code of conduct

As we develop and maintain this project, we aim at keeping its community as open and welcoming as possible. Stay respectful of anyone who would like to contribute to it.

We do not tolerate:

* Harassment and discrimination of any kind
* Sexualized language and imagery
* Personnal attacks
* Publishing other people's private information without explicit consent
* Spoiler for any show or movie

Any contribution, message, comment, question, code, issue, or commit that would not respect this code will be removed, regardless of its content.

If you have found any contribution to be offensive, or that would break this conde, please contact us so that we can remove it.

## Contact

If you want to contribute, or have any question, you can join us by mail at bastien@treviz.xyz. We usually reply in less than a day.

