module.exports = {
    title: 'Treviz Documentation',
    description: 'Easily deploy and maintain your Treviz instance',
    themeConfig: {
        nav: [
            { text: 'Website', link: 'https://treviz.xyz' },
            { text: 'Explore the platform', link: 'https://app.treviz.org' },
            { text: 'Gitlab', link: 'https://gitlab.com/treviz' },
        ],
        sidebar: [
            '/',
            '/installation',
            '/how-to-use',
            '/administration',
            '/api',
            '/how-to-contribute',
            '/license'
        ]
    }
}