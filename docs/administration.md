# Administrating the platform

## Creating the first admin user

If you just created your first Treviz instance, no user should be running yet. You need to create one to log and administer future
users. Connect to the web server the API is running on. If you installed Treviz with Docker, please connect to it by executing

```
sudo docker exec -it api bash
```

You can now create the admin user with the following CLI:

```
php bin/console fos:user:create <admin-username> <admin-email> <admin-password> --super-admin
```

Navigate to the frontend of the platform, and log with the credentials you just gave.

## Using the administration interface

The administration interface can be found in *Settings* -> *Administration*. From there, you can:
* create, update and delete users from the platform
* specify "Organizations" your users may be linked to
* Whitelist or Blacklist email domains to filter the users that can register on the platform