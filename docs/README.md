# Treviz Documentation

## Introduction

Treviz is an open-source web platform that helps you set up and join open, collaborative organizations, in which you can work on the projects you want and get rewarded for your work.

## How it Works

Treviz is made of three modules:
* [treviz-frontend](https://gitlab.com/treviz/treviz-front): an [Angular 7](https://angular.io) application
* [treviz-backend](https://gitlab.com/treviz/treviz-back): the API that powers the platform, built with [Symfony 3.4](https://symfony.com/)
* [treviz-events](https://gitlab.com/treviz/treviz-events): a Node.Js microservice that is used to send real-time notifications

Other modules include the [treviz-website](https://gitlab.com/treviz/treviz-website), which is mainly a showcase for the product, and 
[this documentation](https://gitlab.com/treviz/treviz-documentation).

**Warning**: Treviz is still in Beta, and this documentation is yet to be completed. Breaking changes may occur, even if we try not to.

## To-Do:

* [x] Installation guide
* [•] Administration guide
* [ ] List of the various functionalities of the platform
* [x] Contribution guide
* [ ] API Specifications and style guide
* [x] License
