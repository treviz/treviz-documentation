FROM node:latest as builder
WORKDIR /var/www/doc

RUN npm i -g vuepress
COPY docs/ ./
RUN vuepress build

FROM nginx:alpine
WORKDIR /usr/share/nginx/html
COPY --from=builder /var/www/doc/.vuepress/dist ./
COPY ./webserver-conf/nginx.conf /etc/nginx/
RUN rm /etc/nginx/conf.d/default.conf
COPY ./webserver-conf/treviz-doc.conf /etc/nginx/conf.d/default.conf